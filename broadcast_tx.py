#!/usr/bin/env python3
#coding: utf-8

from socket import *
import time

# setup destinate address
dest = ('255.255.255.255',9090)
broadcast_socket = socket(AF_INET,SOCK_DGRAM)

# enable for tx
broadcast_socket.setsockopt(SOL_SOCKET,SO_BROADCAST,True)

# start to send
while True:
    time.sleep(2)
    print('Start to send .....')
    content = "TX: {}".format(time.time())
    broadcast_socket.sendto(content.encode("utf-8"), dest)
s.close()
