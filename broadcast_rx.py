#!/usr/bin/env python3
#coding: utf-8

from socket import *

HOST = '255.255.255.255'
PORT = 9090
ADDR = (HOST,PORT)

# Create socket
udp_socket = socket(AF_INET,SOCK_DGRAM)
udp_socket.bind(ADDR)

# Start to receive
while True:
   try:
       data,addr = udp_socket.recvfrom(1024)
       print('[RX] {}'.format(data.decode("utf-8")))
   except (KeyboardInterrupt, SyntaxError):
       raise
   except Exception as e:
       print(e)
udp_socket.close()
