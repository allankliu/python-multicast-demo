# Readme

multicast-demo is used for broadcast the internal status of gateway.
Since multicast-demo demands special packet filter disabled in the firewall, we decided to use the broadcast method
as the alarm pushing method inside a LAN/WLAN.

- https://www.programcreek.com/python/example/4400/socket.SO_BROADCAST
- https://www.jianshu.com/p/831c5b3c60c9
